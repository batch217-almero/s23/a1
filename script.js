//console.log("Test");
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,

	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},

	faint: function(){
		console.log("pokemon fainted");
	}
}





// STEP 4- Initialize trainer object
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	// STEP 5- talk method
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);


// STEP 6 - Access trainer object using dot and square brackets
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

// STEP 7-Invoke talk()
trainer.talk();

// STEP 8- Construct a Pokemon
function Pokemon(name, level){
	
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//STEP 10 - tackle method
	this.tackle = function(target){
		target.health = target.health - this.attack;
		console.log(this.name + " tackled " + target.name);
		console.log( target.name + "'s health is now reduced to " + target.health);

		// STEP 12 - invoke faint if health is low
		if(target.health <= 0){
			this.faint(target);
		}
	}

	//STEP 11 - faint method
	this.faint = function(target){
		console.log( target.name + " fainted");
	}
}

// STEP 9 - Create various Pokemon object
let pokemon1 = new Pokemon("Pikachu", 12);
let pokemon2 = new Pokemon("Geodude", 8);
let pokemon3 = new Pokemon("Mewtwo", 100);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon2.tackle(pokemon1);
console.log(pokemon1);

pokemon3.tackle(pokemon2);
console.log(pokemon2);